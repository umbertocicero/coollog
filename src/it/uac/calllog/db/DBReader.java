package it.uac.calllog.db;

import it.uac.calllog.R;
import it.uac.calllog.bo.CallObject;
import it.uac.calllog.comparator.DateComparator;
import it.uac.calllog.util.Costants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.util.Log;
import android.util.SparseArray;

@SuppressLint("InlinedApi")
public class DBReader {

	static final String[] projection = new String[] { Contacts.DISPLAY_NAME, Contacts._ID };
	Context context;

	public DBReader(Context context) {
		super();
		this.context = context;
	}

	public SparseArray<ArrayList<CallObject>> getAllCalls() {
		HashMap<String, CallObject> contactMap = new HashMap<String, CallObject>();
		String[] projection = new String[] { Data._ID, Phone.NORMALIZED_NUMBER, Contacts.DISPLAY_NAME_PRIMARY, Contacts.DISPLAY_NAME, ContactsContract.Data.CONTACT_ID };
		Cursor nameCursor = context.getContentResolver().query(Data.CONTENT_URI, projection, null, null, null);
		try {
			nameCursor.moveToFirst();

			if (!nameCursor.isAfterLast()) {
				do {
					int name = nameCursor.getColumnIndex(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.DISPLAY_NAME_PRIMARY : Contacts.DISPLAY_NAME);
					int contact = nameCursor.getColumnIndex(Data._ID);
					int num = nameCursor.getColumnIndex(Phone.NORMALIZED_NUMBER);
					int photoId = nameCursor.getColumnIndex(ContactsContract.Data.CONTACT_ID);// ContactsContract.Data.RAW_CONTACT_ID
					String numString = nameCursor.getString(num);

					CallObject temp = new CallObject();
					temp.setName(nameCursor.getString(name));
					temp.setPhNumber(numString);
					temp.setContactId(nameCursor.getString(contact));
					temp.setIdPhoto(Integer.valueOf(nameCursor.getString(photoId)));

					contactMap.put(numString, temp);

				} while (nameCursor.moveToNext());
			}
		} catch (Exception ex) {
			Log.e("DB Error: first cursor", ex.getMessage());
		} finally {
			if (nameCursor != null && !nameCursor.isClosed()) {
				nameCursor.close();
			}
		}

		ArrayList<CallObject> allCall = new ArrayList<CallObject>();
		ArrayList<CallObject> missedCall = new ArrayList<CallObject>();
		ArrayList<CallObject> inCall = new ArrayList<CallObject>();
		ArrayList<CallObject> outCall = new ArrayList<CallObject>();
		CallObject call;

		Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " " + DateComparator.DESC);

		int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
		int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
		int date = cursor.getColumnIndex(CallLog.Calls.DATE);
		int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);

		try {
			cursor.moveToFirst();

			if (!cursor.isAfterLast()) {
				do {

					String phNumber = cursor.getString(number);
					String callType = cursor.getString(type);
					String callDate = cursor.getString(date);
					Date callDayTime = new Date(Long.valueOf(callDate));
					String callDuration = cursor.getString(duration);

					call = new CallObject();

					CallObject obj = contactMap.get(phNumber);

					if (obj != null) {
						call.setContactId(obj.getContactId());
						call.setName((obj.getName() != null || "".equals(obj.getName())) ? obj.getName() : phNumber);
						call.setIdPhoto(obj.getIdPhoto());
					} else {
						call.setName(phNumber);
					}
					call.setPhNumber(phNumber);

					call.setCallDate(callDate);
					call.setCallDayTime(callDayTime);
					call.setCallDuration(callDuration);

					String dir = null;
					int dircode = Integer.parseInt(callType);
					call.setCallType(dircode);
					boolean known = true;
					switch (dircode) {
						case CallLog.Calls.OUTGOING_TYPE:
							dir = context.getString(R.string.outcoming);
							call.setDir(dir);
							outCall.add(call);
							break;

						case CallLog.Calls.INCOMING_TYPE:
							dir = context.getString(R.string.incoming);
							call.setDir(dir);
							inCall.add(call);
							break;

						case CallLog.Calls.MISSED_TYPE:
							dir = context.getString(R.string.missed);
							call.setDir(dir);
							missedCall.add(call);
							break;

						default:
							Log.e("Unknow Dir", String.format("Direction: %s - Name: %s - Number: %s - Date: %s", dircode, obj != null ? obj.getName() : phNumber, phNumber, callDayTime));
							known = false;
							break;
					}

					call.setDir(dir);
					if (known)
						allCall.add(call);
				} while (cursor.moveToNext());
			}
		} catch (SQLException e) {
			Log.e("DB Error", e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (cursor != null && !cursor.isClosed())
					cursor.close();
			} catch (Exception ex) {
				Log.e("DB Error", ex.getMessage());
			}
		}

		SparseArray<ArrayList<CallObject>> map = new SparseArray<ArrayList<CallObject>>();
		map.put(Costants.ALL, allCall);
		map.put(Costants.MISSED_TYPE, missedCall);
		map.put(Costants.INCOMING_TYPE, inCall);
		map.put(Costants.OUTGOING_TYPE, outCall);

		return map;
	}

	public SparseArray<ArrayList<CallObject>> getFAKEAllCalls() {
		SparseArray<ArrayList<CallObject>> map = new SparseArray<ArrayList<CallObject>>();

		ArrayList<CallObject> allCall = new ArrayList<CallObject>();
		ArrayList<CallObject> missedCall = new ArrayList<CallObject>();
		ArrayList<CallObject> inCall = new ArrayList<CallObject>();
		ArrayList<CallObject> outCall = new ArrayList<CallObject>();

		String[] name = { "Alice", "Bob", "Charlie", "Tom", "Emily", "Hannah", "Madison", "Ashley" };
		String[] number = { "+1 202-855-9988", "+1 202-887-5399", "+1 202-867-5509", "+1 202-769-7779", "+1 672-87-7779", "+1 276-999-0079", "+1 882-899-6779", "+1 112-989-6779" };

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		for (int i = 0; i < 211; i++) {
			CallObject obj = new CallObject();
			int j = (int) (Math.random() * 10) % 8;
			obj.setName(name[j]);
			obj.setPhNumber(number[j]);

			cal.add(Calendar.MINUTE, -(int) (Math.random() * 360));
			String callDate = null;
			callDate = String.valueOf(Long.valueOf(cal.getTimeInMillis()));
			obj.setCallDate(callDate);
			Date callDayTime = new Date(Long.valueOf(callDate));
			obj.setCallDayTime(callDayTime);

			obj.setCallDuration("" + (int) (Math.random() * 3600));

			String dir = null;
			int dircode = (int) ((Math.random() * 3) + 1);
			obj.setCallType(dircode);
			switch (dircode) {
				case CallLog.Calls.OUTGOING_TYPE:
					dir = context.getString(R.string.outcoming);
					obj.setDir(dir);
					outCall.add(obj);
					break;

				case CallLog.Calls.INCOMING_TYPE:
					dir = context.getString(R.string.incoming);
					obj.setDir(dir);
					inCall.add(obj);
					break;

				case CallLog.Calls.MISSED_TYPE:
					dir = context.getString(R.string.missed);
					obj.setDir(dir);
					missedCall.add(obj);
					break;

				default:
					break;
			}
			obj.setDir(dir);
			allCall.add(obj);

			map.put(Costants.ALL, allCall);
			map.put(Costants.MISSED_TYPE, missedCall);
			map.put(Costants.INCOMING_TYPE, inCall);
			map.put(Costants.OUTGOING_TYPE, outCall);
		}
		return map;
	}
}
