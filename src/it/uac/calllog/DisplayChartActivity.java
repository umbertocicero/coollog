package it.uac.calllog;

import it.uac.calllog.adapter.LegendAdapter;
import it.uac.calllog.bo.CallObject;
import it.uac.calllog.bo.DataObject;
import it.uac.calllog.menu.ChartSettingsActivity;
import it.uac.calllog.singleton.MapSingleton;
import it.uac.calllog.util.Costants;
import it.uac.calllog.util.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TableLayout;

public class DisplayChartActivity extends BaseActivity {
	float values[] = { 300, 400, 100, 500 };
	GraphicalView mChartView = null;
	CategorySeries distributionSeries;
	DefaultRenderer defaultRenderer;

	private Button numberButton;
	private Button durationButton;
	private LegendAdapter adapter;
	private ListView listView;

	int typeChart = Costants.CHART_NUMBER;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_chart);

		Intent intent = getIntent();
		final int statisticType = intent.getIntExtra(Costants.STATISTICS_TYPE, Costants.PREF_DEFAULT_TAB);
		String statisticString = prefs.getString(Costants.STATISTICS_TYPE_NAME, getString(R.string.app_name));
		setTitle(statisticString);

		numberButton = (Button) findViewById(R.id.numberButton);
		durationButton = (Button) findViewById(R.id.durationButton);
		changeAdapterValue(numberButton);

		numberButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				typeChart = Costants.CHART_NUMBER;
				openChart(statisticType, typeChart);
				changeAdapterValue(numberButton);
			}
		});
		durationButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				typeChart = Costants.CHART_DURATION;
				openChart(statisticType, typeChart);
				changeAdapterValue(durationButton);
			}
		});

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			TableLayout tab = (TableLayout) findViewById(R.id.table);
			tab.setBackgroundResource(R.drawable.shape_blue_white);
		}
		openChart(statisticType, typeChart);
	}

	private ArrayList<DataObject> chart(int statisticType, int typeChart) {
		ArrayList<DataObject> ret = new ArrayList<DataObject>();
		SparseArray<ArrayList<CallObject>> map = MapSingleton.getInstance().getMap(this);

		boolean inVsOut = false;
		ArrayList<CallObject> list = null;
		switch (statisticType) {
		case Costants.ALL:
			list = map.get(statisticType);
			break;
		case Costants.MISSED_TYPE:
			list = map.get(statisticType);
			durationButton.setVisibility(View.GONE);
			View middleView = (View) findViewById(R.id.middleView);
			middleView.setVisibility(View.GONE);
			break;
		case Costants.INCOMING_TYPE:
			list = map.get(statisticType);
			break;
		case Costants.OUTGOING_TYPE:
			list = map.get(statisticType);
			break;
		case Costants.IN_VS_OUT_TYPE:
			list = map.get(Costants.ALL);
			inVsOut = true;
			break;
		default:
			break;
		}

		Map<String, Long> mapCallNumber = new HashMap<String, Long>();

		String seriesName = null;
		for (CallObject callObject : list) {
			if (inVsOut) {
				if (callObject.getCallType() == CallLog.Calls.OUTGOING_TYPE) {
					seriesName = getString(R.string.outcoming);
				} else if (callObject.getCallType() == CallLog.Calls.INCOMING_TYPE) {
					seriesName = getString(R.string.incoming);
				}
			} else {
				seriesName = callObject.getName();
			}

			if (seriesName != null) {
				Long value = mapCallNumber.get(seriesName);
				Long time = mapCallNumber.get(seriesName);
				if (value != null) {
					value += 1;
					time += Long.valueOf(callObject.getCallDuration());
				} else {
					value = 1l;
					time = Long.valueOf(callObject.getCallDuration());
				}
				if (typeChart == Costants.CHART_NUMBER)
					mapCallNumber.put(seriesName, value);
				else
					mapCallNumber.put(seriesName, time);
			}
		}

		for (Entry<String, Long> entry : mapCallNumber.entrySet()) {
			String key = entry.getKey();
			Long value = entry.getValue();
			ret.add(new DataObject(key, value));
		}
		Collections.sort(ret, new Comparator<DataObject>() {
			@Override
			public int compare(DataObject obj1, DataObject obj2) {
				return obj2.getValue().compareTo(obj1.getValue());
			}
		});
		return ret;
	}

	private void openChart(int statisticType, int typeChart) {
		// Instantiating CategorySeries to plot Pie Chart
		distributionSeries = new CategorySeries("");
		final ArrayList<DataObject> dataObjectList = chart(statisticType, typeChart);
		for (DataObject dataObject : dataObjectList) {
			String value = "";
			if (Costants.CHART_DURATION == typeChart)
				value = Utilities.formatTime(dataObject.getValue(), false);
			else
				value = String.valueOf(dataObject.getValue());
			distributionSeries.add(dataObject.getName() + " (" + value + ")", dataObject.getValue());
		}

		// Instantiating a renderer for the Pie Chart
		defaultRenderer = new DefaultRenderer();
		for (int i = 0; i < dataObjectList.size(); i++) {
			SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
			// seriesRenderer.setColor(colors[i]);
			seriesRenderer.setColor(Utilities.getRandomColor(this, i));
			seriesRenderer.setDisplayBoundingPoints(true);
			seriesRenderer.setHighlighted(false);
			seriesRenderer.setShowLegendItem(false);
			seriesRenderer.setShowLabelItem(false);
			seriesRenderer.setStroke(BasicStroke.SOLID);
			// Adding a renderer for a slice
			defaultRenderer.addSeriesRenderer(seriesRenderer);
		}

		defaultRenderer.setZoomButtonsVisible(false);
		defaultRenderer.setLabelsTextSize(Utilities.dpToPx(14, this));
		defaultRenderer.setLabelsColor(Color.BLACK);
		defaultRenderer.setDisplayValues(false);
		defaultRenderer.setFitLegend(false);
		defaultRenderer.setShowLegend(false);
		defaultRenderer.setSelectableBuffer(100);
		defaultRenderer.setClickEnabled(true);
		defaultRenderer.setShowTickMarks(false);
		defaultRenderer.setMargins(new int[] { 0, 0, 0, 0 });

		defaultRenderer.setScale(1.35f);
//		defaultRenderer.setPanEnabled(false);
		// defaultRenderer.setZoomEnabled(prefs.getBoolean(Costants.PREF_ZOOM_ENABLED, Costants.PREF_ZOOM_ENABLED_DEF));
		// defaultRenderer.setShowLabels(prefs.getBoolean(Costants.PREF_DISPLAY_NAME, Costants.PREF_DISPLAY_NAME_DEF));

		LinearLayout layout = (LinearLayout) findViewById(R.id.graphicalContent);

		if (mChartView != null) {
			((ViewManager) layout.getParent()).removeView(mChartView);
			((ViewManager) mChartView.getParent()).removeView(mChartView);
			mChartView = null;

		}
		if (mChartView == null) {
			mChartView = ChartFactory.getPieChartView(getBaseContext(), distributionSeries, defaultRenderer);

			mChartView.setClickable(true);

			if (prefs.getBoolean(Costants.PREF_SINGLE_CLICK, Costants.PREF_SINGLE_CLICK_DEF)) {
				mChartView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						clickManager();
					}
				});
			} else {
				mChartView.setOnLongClickListener(new View.OnLongClickListener() {
					public boolean onLongClick(View v) {
						// clickManager(); //FIXME
						return true;
					}
				});
			}

		} else {
			mChartView.repaint();

		}

		layout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		adapter = new LegendAdapter(this, typeChart, null, android.R.layout.simple_list_item_1, dataObjectList);

		listView = (ListView) findViewById(R.id.listViewLegend);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				for (int i = 0; i < distributionSeries.getItemCount(); i++) {
					defaultRenderer.getSeriesRendererAt(i).setHighlighted(i == position);
					defaultRenderer.getSeriesRendererAt(i).setShowLabelItem(i == position);
				}
				mChartView.repaint();

				adapter.setSelectedItem(position);
				adapter.notifyDataSetChanged();// To refresh item inside list view
			}

		});

		listView.setAdapter(adapter);
	}

	private void changeAdapterValue(Button button) {
		numberButton.setTextAppearance(this, R.style.DefaultTableText);
		durationButton.setTextAppearance(this, R.style.DefaultTableText);
		button.setTextAppearance(this, R.style.BluHUGETableText);
	}

	private void clickManager() {
		// handle the click event on the chart
		SeriesSelection seriesSelection = mChartView.getCurrentSeriesAndPoint();
		if (seriesSelection != null) {
			// display information of the clicked point
			for (int i = 0; i < distributionSeries.getItemCount(); i++) {
				defaultRenderer.getSeriesRendererAt(i).setHighlighted(i == seriesSelection.getPointIndex());
				defaultRenderer.getSeriesRendererAt(i).setShowLabelItem(i == seriesSelection.getPointIndex());
			}
			mChartView.repaint();

			adapter.setSelectedItem(seriesSelection.getPointIndex());
			adapter.notifyDataSetChanged();// To refresh item inside list view
			listView.requestFocusFromTouch(); // IMPORTANT!
			listView.setSelection(seriesSelection.getPointIndex());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.chart_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent settingsIntent = new Intent(this, ChartSettingsActivity.class);
			startActivity(settingsIntent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (defaultRenderer != null) {
			defaultRenderer.setZoomEnabled(prefs.getBoolean(Costants.PREF_ZOOM_ENABLED, Costants.PREF_ZOOM_ENABLED_DEF));
			defaultRenderer.setShowLabels(prefs.getBoolean(Costants.PREF_DISPLAY_NAME, Costants.PREF_DISPLAY_NAME_DEF));
			defaultRenderer.setPanEnabled(prefs.getBoolean(Costants.PREF_MOVE_ENABLED, Costants.PREF_MOVE_ENABLED_DEF));
		}
	}
}
