package it.uac.calllog.asyncTask;

import it.uac.calllog.bo.CallObject;
import it.uac.calllog.bo.DataObject;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.widget.TextView;

public class StatisticsTask extends AsyncTask<Void, Object, DataObject> {
	TextView textView;
	ArrayList<CallObject> list;
	String nameSelected;

	public StatisticsTask(TextView textView, ArrayList<CallObject> list, String nameSelected) {
		super();
		this.textView = textView;
		this.list = list;
		this.nameSelected = nameSelected;
	}

	@Override
	protected DataObject doInBackground(Void... params) {
		DataObject ret = new DataObject();
		Long count = 0l;
		if (list.size() == 0) {
			ret.setValue(count);
			ret.setPecentage(0);
			return ret;
		}
		String currentName = null;
		for (CallObject callObject : list) {
			currentName = callObject.getName();
			if (currentName != null && currentName.equals(nameSelected)) {
				count += 1;
			}
		}
		ret.setValue(count);
		double percent = ((double)count/(double)list.size())*100;
		ret.setPecentage(percent);
		return ret;
	}

	@Override
	protected void onPostExecute(DataObject result) {
		super.onPostExecute(result);
		textView.setText(String.format("%s / %,.2f%%", result.getValue(),result.getPecentage()));
	}

}
