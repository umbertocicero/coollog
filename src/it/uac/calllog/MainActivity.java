package it.uac.calllog;

import it.uac.calllog.fragment.ShowCallsFragment;
import it.uac.calllog.fragment.StatisticsFragment;
import it.uac.calllog.singleton.MapSingleton;
import it.uac.calllog.util.Costants;
import it.uac.calllog.util.Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements ActionBar.TabListener {
	private final String TAG = "it.uac.calllog.MainActivity";
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;
	AlertDialog alertDialog;
	AlertDialog alertDialogRate;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	private TextView lastUpdate;

	SharedPreferences prefs;
	SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lastUpdate = (TextView) findViewById(R.id.lastUpdate);
		lastUpdate.setText(String.format("%s: %s", getString(R.string.last_update), MapSingleton.getInstance().getLastUpdate(this)));

		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Force menu overflow icon
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception ex) {
			// Ignore exceptions
		}

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab().setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
		}

		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		
		showRate();
//		if (alertDialogRate == null || !alertDialogRate.isShowing())
//		getLastVersion();
	}

	private void showRate(){
		Long accessNumber = prefs.getLong(Costants.ACCESS_NUMBER, 1);
		if (prefs.getBoolean(Costants.SHOW_DIALOG, true)) {
			accessNumber += 1;
			editor = prefs.edit();
			editor.putLong(Costants.ACCESS_NUMBER, accessNumber);
			if ((accessNumber % 8 == 0)) {
				new AlertDialog.Builder(this).setTitle(R.string.vote).setMessage(R.string.vote_text).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						editor.putBoolean(Costants.SHOW_DIALOG, false).commit();
						Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Costants.APP_URL));
						startActivity(i);
					}
				}).setNeutralButton(R.string.not_show, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						editor.putBoolean(Costants.SHOW_DIALOG, false).commit();
					}
				}).setNegativeButton(R.string.askme_later, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// close dialog
					}
				}).setIcon(R.drawable.ico_star_yellow).show();

			}
			editor.commit();
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			refreshMethod(item);
			return true;
		case R.id.action_info:
			startActivity(new Intent(MainActivity.this, InfoActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void refreshMethod(final MenuItem refreshItem) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_ico, null);

		Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_refresh);
		rotation.setRepeatCount(Animation.INFINITE);
		iv.startAnimation(rotation);

		MenuItemCompat.setActionView(refreshItem, iv);
		baseRefreshMethod(refreshItem);

	}

	@SuppressLint("NewApi")
	public void baseRefreshMethod(final MenuItem refreshItem) {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				MapSingleton.getInstance().refreshMap(MainActivity.this);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				if (Utilities.notifierTabCalls != null) {
					Utilities.notifierTabCalls.notify(null);
				}
				if (Utilities.notifierTabStats != null) {
					Utilities.notifierTabStats.notify(null);
				}
				MenuItemCompat.getActionView(refreshItem).clearAnimation();
				MenuItemCompat.setActionView(refreshItem, null);
				lastUpdate.setText(String.format("%s: %s", getString(R.string.last_update), MapSingleton.getInstance().getLastUpdate(MainActivity.this)));
			}

		};

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			task.execute();
		}
	}

	String url;
	private boolean promptUpdate = false;

	@SuppressLint("NewApi")
	public void getLastVersion() {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				String appData = getAppData();
				try {
					// Creation of json object
					JSONObject json = new JSONObject(appData);

					promptUpdate = isVersionUpdated(json.getString("softwareVersion"));

					long now = System.currentTimeMillis();
					if (promptUpdate && now > prefs.getLong(Costants.PREF_LAST_UPDATE_CHECK, 0) + Costants.UPDATE_MIN_FREQUENCY) {
						promptUpdate = true;
						prefs.edit().putLong(Costants.PREF_LAST_UPDATE_CHECK, now).commit();
					} else {
						promptUpdate = false;
					}

				} catch (Exception e) {
					Log.e(TAG, e.getMessage());
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				if (promptUpdate) {
					final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
					alertDialogBuilder.setCancelable(false).setMessage(R.string.new_update_available).setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int id) {
							prefs.edit().putBoolean(Costants.SHOW_DIALOG, false).commit();
							Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Costants.APP_URL));
							startActivity(i);
							dialog.dismiss();
						}
					}).setNegativeButton(R.string.not_now, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
					alertDialog = alertDialogBuilder.create();
					alertDialog.show();
				}
			}

		};

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			task.execute();
		}
	}

	private boolean isVersionUpdated(String playStoreVersion) throws NameNotFoundException {

		boolean result = false;

		// Retrieval of installed app version
		PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
		String installedVersion = pInfo.versionName;

		// Parsing version string to obtain major.minor.point (excluding eventually beta)
		String[] playStoreVersionArray = playStoreVersion.split("b")[0].split("\\.");
		String[] installedVersionArray = installedVersion.split("b")[0].split("\\.");

		// Versions strings are converted into integer
		String playStoreVersionString = playStoreVersionArray[0];
		String installedVersionString = installedVersionArray[0];
		for (int i = 1; i < playStoreVersionArray.length; i++) {
			playStoreVersionString += String.format("%02d", Integer.parseInt(playStoreVersionArray[i]));
			installedVersionString += String.format("%02d", Integer.parseInt(installedVersionArray[i]));
		}

		// And then compared
		if (Integer.parseInt(playStoreVersionString) > Integer.parseInt(installedVersionString)) {
			result = true;
		}

		// And then compared again to check if we're out of Beta
		else if (Integer.parseInt(playStoreVersionString) == Integer.parseInt(installedVersionString) && playStoreVersion.split("b").length == 1 && installedVersion.split("b").length == 2) {
			result = true;
		}

		return result;
	}
	public String getAppData() {
		StringBuilder sb = new StringBuilder();

		if (this != null && !this.isFinishing()) {

			try {
				// get URL content
				URL url = new URL(Costants.PS_METADATA_FETCHER_URL + Costants.APP_URL);
				URLConnection conn = url.openConnection();

				// open the stream and put it into BufferedReader
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

				String inputLine;

				while ((inputLine = br.readLine()) != null) {
					sb.append(inputLine);
				}

			} catch (MalformedURLException e) {
				Log.e(TAG, e.getMessage());
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			}
		}

		return sb.toString();
	}
	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new ShowCallsFragment();
				break;
			case 1:
				fragment = new StatisticsFragment();
				break;
			default:
				break;
			}

			return fragment;
		}

		@Override
		public int getCount() {
			// Show 2 total pages.
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.call).toUpperCase(l);
			case 1:
				return getString(R.string.statistics).toUpperCase(l);
			}
			return null;
		}
	}

//	@Override
//	public void onTabReselected(Tab tab, android.support.v4.app.FragmentTransaction arg1) {
//	}
//
//	@Override
//	public void onTabSelected(Tab tab, android.support.v4.app.FragmentTransaction arg1) {
//		mViewPager.setCurrentItem(tab.getPosition());
//	}
//
//	@Override
//	public void onTabUnselected(Tab tab, android.support.v4.app.FragmentTransaction arg1) {
//	}

	@Override
	protected void onPause() {
		if (alertDialog != null && alertDialog.isShowing()) {
			alertDialog.dismiss();
		}
		super.onPause();
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction arg1) {
		mViewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

}
