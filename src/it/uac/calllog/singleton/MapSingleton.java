package it.uac.calllog.singleton;

import it.uac.calllog.bo.CallObject;
import it.uac.calllog.db.DBReader;
import it.uac.calllog.util.Costants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

public class MapSingleton {

	private static MapSingleton instance;
	private static String TAG = "Singleton";
	private SparseArray<ArrayList<CallObject>> map;
	private String lastUpdate;

	private MapSingleton() {
		// Constructor hidden because this is a singleton
	}

	public static void initInstance() {
		if (instance == null) {
			// Create the instance
			instance = new MapSingleton();
			Log.i(TAG, "Init Singleton");
		}
	}

	public static MapSingleton getInstance() {
		// Return the instance
		initInstance();
		return instance;
	}

	public void initMapSingletonMethod(Context context) {
		// Custom method
		if (map == null) {
			DBReader reader = new DBReader(context);
			map = reader.getAllCalls();
			// map = reader.getFAKEAllCalls();
		}
		if (lastUpdate == null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss",Locale.getDefault());
			lastUpdate = sdf.format(new Date(System.currentTimeMillis()));
		}
	}

	public SparseArray<ArrayList<CallObject>> getMap(Context context) {
		initMapSingletonMethod(context);
		return map;
	}

	public String getLastUpdate(Context context) {
		initMapSingletonMethod(context);
		return lastUpdate;
	}

	public void refreshMap(Context context) {
		DBReader reader = new DBReader(context);
		map = reader.getAllCalls();
		SimpleDateFormat sdf = new SimpleDateFormat(Costants.DATA_FORMAT,Locale.getDefault());
		lastUpdate = sdf.format(new Date(System.currentTimeMillis()));
	}
}
