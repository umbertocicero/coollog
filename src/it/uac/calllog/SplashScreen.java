package it.uac.calllog;

import it.uac.calllog.singleton.MapSingleton;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Log;

public class SplashScreen extends Activity{

	private static int SPLASH_TIME_OUT = 1300;
	private static final boolean DEVELOPER_MODE = false;
	private static String TAG = "it.uac.calllog";
	private long startTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		strictMode();
		Log.d(TAG, "START SPLASH");
		startTime = System.currentTimeMillis();
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onResume() {
		super.onResume();
		PrefetchData task = new PrefetchData();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			task.execute();
		}
	}
	
	private void strictMode() {
		if (DEVELOPER_MODE) {
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork() // or .detectAll()
																																	// for all
																																	// detectable
																																	// problems
					.penaltyLog().build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().penaltyLog().penaltyDeath().build());
		}
	}
	
	
	private class PrefetchData extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			try {
				MapSingleton.getInstance().initMapSingletonMethod(SplashScreen.this);
			} catch (Exception ex) {
				Log.e(TAG, ex.toString());
			} finally {
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void unused) {
			long newTime = System.currentTimeMillis();
			long sleep = newTime - startTime;
			Log.d(TAG, "onPostExecute: tot time = " + sleep + "ms");
			if (sleep < SPLASH_TIME_OUT)
				SystemClock.sleep(SPLASH_TIME_OUT - sleep);
			Intent i = new Intent(SplashScreen.this, MainActivity.class);
			startActivity(i);
			finish();
		}
	}
}
