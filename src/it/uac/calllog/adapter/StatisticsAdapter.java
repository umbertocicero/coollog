package it.uac.calllog.adapter;

import it.uac.calllog.R;
import it.uac.calllog.bo.DataObject;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StatisticsAdapter extends ArrayAdapter<DataObject> {

	private LayoutInflater inflater;
	private Activity context;
	private Integer selectedItem;

	public StatisticsAdapter(Activity context, int textViewResourceId) {
		super(context, textViewResourceId);
		inflater = LayoutInflater.from(context);
	}

	public StatisticsAdapter(Activity context, Integer selectedItem, int textViewResourceId, ArrayList<DataObject> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.selectedItem = selectedItem;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewOptimize(position, convertView, parent);
	}

	public View getViewOptimize(int position, View convertView, ViewGroup parent) {
		final CallListViewCache viewCache;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.statistics_object, null);

			viewCache = new CallListViewCache(convertView);

			convertView.setTag(viewCache);
		} else {
			convertView = (RelativeLayout) convertView;
			viewCache = (CallListViewCache) convertView.getTag();
		}

		TextView name = viewCache.getName();
		TextView value = viewCache.getValue();

		final DataObject data = getItem(position);
		if (selectedItem != null) {
			if (selectedItem == position) {
				convertView.setBackgroundColor(context.getResources().getColor(R.color.blue_light));
			} else {
				convertView.setBackgroundColor(0);
			}
		}

		name.setText(data.getName());
		value.setText(data.getValue() != null ? String.valueOf(data.getValue()) : "");
		return convertView;
	}

	public void setSelectedItem(Integer selectedItem) {
		this.selectedItem = selectedItem;
	}

}
