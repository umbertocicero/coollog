package it.uac.calllog.adapter;

import it.uac.calllog.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CallListViewCache {
	private View baseView;
	
	private TextView name;
	private TextView number;
	private TextView value;
	private TextView date;
	private ImageView  ico;
	private Bitmap outoing;
	
	public CallListViewCache ( View baseView ) {
        this.baseView = baseView;
    }
	
	public TextView getName () {
        if ( name == null ) {
        	name = ( TextView ) baseView.findViewById(R.id.textViewName);
        }
        return name;
    }
	public TextView getValue () {
		if ( value == null ) {
			value = ( TextView ) baseView.findViewById(R.id.textViewValue);
		}
		return value;
	}
	public TextView getNumber () {
		if ( number == null ) {
			number = ( TextView ) baseView.findViewById(R.id.textViewNumber);
		}
		return number;
	}
	public TextView getDate () {
		if ( date == null ) {
			date = ( TextView ) baseView.findViewById(R.id.textViewDate);
		}
		return date;
	}
	
	public ImageView getIco () {
        if ( ico == null ) {
        	ico = ( ImageView ) baseView.findViewById(R.id.icon);
        }
        return ico;
    }

	public Bitmap getOutgoing(){
		if ( outoing == null ) {
			outoing = BitmapFactory.decodeResource(baseView.getResources(), R.drawable.ic_outgoing);
        }
        return outoing;
	}
}
