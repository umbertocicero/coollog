package it.uac.calllog.adapter;

import it.uac.calllog.R;
import it.uac.calllog.bo.CallObject;
import it.uac.calllog.comparator.DateComparator;
import it.uac.calllog.util.Costants;
import it.uac.calllog.util.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.provider.CallLog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CallAdapter extends ArrayAdapter<CallObject> {
	private LayoutInflater inflater;
	private boolean completeDate;
	private Integer selectedItem;
	private Activity context;

	public CallAdapter(Activity context, int textViewResourceId, boolean completeDate) {
		super(context, textViewResourceId);
		inflater = LayoutInflater.from(context);
	}

	public CallAdapter(Activity context, boolean completeDate, Integer selectedItem, int textViewResourceId, ArrayList<CallObject> objects) {

		super(context, textViewResourceId, objects);
		this.context = context;
		this.completeDate = completeDate;
		this.selectedItem = selectedItem;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewOptimize(position, convertView, parent);
	}

	public View getViewOptimize(int position, View convertView, ViewGroup parent) {
		final CallListViewCache viewCache;
		Resources res = getContext().getResources();

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.call_object, null);

			viewCache = new CallListViewCache(convertView);

			convertView.setMinimumHeight(48);
			convertView.setTag(viewCache);
		} else {
			convertView = (RelativeLayout) convertView;
			viewCache = (CallListViewCache) convertView.getTag();
		}

		TextView name = viewCache.getName();
		TextView phNumber = viewCache.getNumber();
		TextView date = viewCache.getDate();
		final ImageView ico = viewCache.getIco();

		final CallObject call = getItem(position);
		if (selectedItem != null) {
			if (selectedItem == position) {
				convertView.setBackgroundColor(context.getResources().getColor(R.color.blue_light));
			} else {
				convertView.setBackgroundColor(0);
			}
		}
		int dircode = call.getCallType();
		if (dircode == CallLog.Calls.OUTGOING_TYPE) {
			Bitmap bm = viewCache.getOutgoing();
			if (bm != null) {
				ico.setImageBitmap(bm);
			}

		} else {
			ico.setImageBitmap(null);
		}
		if (dircode == CallLog.Calls.MISSED_TYPE) {
			name.setTextColor(res.getColor(R.color.red));
		} else {
			name.setTextColor(phNumber.getTextColors());
		}
		name.setText(!call.getName().equals(call.getPhNumber()) ? call.getName() : res.getString(R.string.unknown));
		phNumber.setText(call.getPhNumber());
		Date d = new Date(Long.valueOf(call.getCallDate()));
		GregorianCalendar greg = new GregorianCalendar();
		
		greg.setTime(d);
		
		String dayOfWeek = "";
		
		boolean complete = false;
		if(!completeDate){
			GregorianCalendar lastWeek = new GregorianCalendar();
			lastWeek.add(Calendar.DATE, -7);
			if (greg.getTime().before(lastWeek.getTime())) {
				complete=true;
			}else{
				dayOfWeek = Utilities.switchDayOfWeek(greg, res);
			}
		} else {
			complete=true;
		}
		if(complete){
			SimpleDateFormat sdf = new SimpleDateFormat(Costants.DATA_FORMAT_NO_YEAR, Locale.getDefault());
			dayOfWeek = sdf.format(greg.getTime());
		}

		date.setText(dayOfWeek);

		return convertView;
	}

	public void setSelectedItem(Integer selectedItem) {
		this.selectedItem = selectedItem;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	// -----------Order the content of the arrayAdapter------------//
	public void sort(String orderType) {
		super.sort(new DateComparator(orderType));
		notifyDataSetChanged();
	}
}
