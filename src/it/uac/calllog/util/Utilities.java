package it.uac.calllog.util;

import it.uac.calllog.R;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.Contacts.People;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.util.DisplayMetrics;

@SuppressWarnings("deprecation")
public class Utilities {
	public static INotifier notifierTabCalls;
	public static INotifier notifierTabStats;

	public static int pxToDp(int px, Context ctx) {
		DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
		int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return dp;
	}

	public static int dpToPx(int dp, Context ctx) {
		DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
		int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	public static synchronized String formatTime(long longVal, boolean withZero) {

		int hours = (int) longVal / 3600;
		int remainder = (int) longVal - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;
		String result = "";
		if (!withZero)
			result = "" + (hours > 0 ? hours + ":" : "") + (mins > 0 ? mins + ":" : "") + secs;
		else {
			String minuteString;
			if (mins > 0)
				if (mins > 9)
					minuteString = mins + ":";
				else
					minuteString = "0" + mins + ":";
			else
				minuteString = "00:";
			result = "" + (hours > 0 ? hours + ":" : "0:") + minuteString + (secs > 9 ? secs : "0" + secs);
		}
		return result;
	}

	public static synchronized String switchDayOfWeek(GregorianCalendar greg, Resources res) {
		String dayOfWeek = "";

		GregorianCalendar current = new GregorianCalendar();
		int day = current.get(Calendar.DAY_OF_MONTH);
		int month = current.get(Calendar.MONTH);
		int year = current.get(Calendar.YEAR);

		if (day == greg.get(Calendar.DAY_OF_MONTH) && month == greg.get(Calendar.MONTH) && year == greg.get(Calendar.YEAR)) {
			dayOfWeek = res.getString(R.string.today);
		} else {
			// get yesterday's date
			current.add(Calendar.DATE, -1);
			// get components of yesterday's date
			int yMonth = current.get(Calendar.MONTH);
			int yDay = current.get(Calendar.DATE);
			int yYear = current.get(Calendar.YEAR);
			if (yDay == greg.get(Calendar.DAY_OF_MONTH) && yMonth == greg.get(Calendar.MONTH) && yYear == greg.get(Calendar.YEAR)) {
				dayOfWeek = res.getString(R.string.yesterday);
			} else {
				switch (greg.get(Calendar.DAY_OF_WEEK)) {
				case Calendar.SUNDAY:
					dayOfWeek = res.getString(R.string.sunday);
					break;
				case Calendar.MONDAY:
					dayOfWeek = res.getString(R.string.monday);
					break;
				case Calendar.TUESDAY:
					dayOfWeek = res.getString(R.string.tuesday);
					break;
				case Calendar.WEDNESDAY:
					dayOfWeek = res.getString(R.string.wednesday);
					break;
				case Calendar.THURSDAY:
					dayOfWeek = res.getString(R.string.thursday);
					break;
				case Calendar.FRIDAY:
					dayOfWeek = res.getString(R.string.friday);
					break;
				case Calendar.SATURDAY:
					dayOfWeek = res.getString(R.string.saturday);
					break;
				default:
					break;
				}
			}

		}

		return dayOfWeek;
	}

	public static int getRandomColor(Context context, int position) {

		position = position > 23 ? position % 23 : position;

		return context.getResources().getColor(RANDOM_COLORS[position]);
	}

	public static int[] RANDOM_COLORS = new int[] { //
	R.color.green_pastel,//
			R.color.red_brick,//
			R.color.orange,//
			R.color.violet_pastel,//

			R.color.blue_pastel_night,//
			R.color.yellow_pastel,//
			R.color.brown,//
			R.color.brown_dark,//
			R.color.green_dark_sea,//
			R.color.yellow_khaki,//

			R.color.green_dark, //
			R.color.blue_dark,//
			R.color.red_dark,//
			R.color.violet_dark,//
			R.color.green_acid,//
			R.color.orange_dark,//

			R.color.yellow_dirty,//
			R.color.brown_chocolate,//

			R.color.gray_light,//
			R.color.pink,//
			R.color.yellow_light,//

			R.color.blue_cadet,//
			R.color.brown_cornsilk,//
			R.color.blue_teal //
	};

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public Bitmap openPhoto(long contactId, Activity activity) {
		Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
		Uri photoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.CONTENT_DIRECTORY);
		Cursor cursor = activity.getContentResolver().query(photoUri, new String[] { Contacts.Photo.PHOTO }, null, null, null);
		if (cursor == null) {
			return null;
		}
		try {
			if (cursor.moveToFirst()) {
				byte[] data = cursor.getBlob(0);
				if (data != null) {
					InputStream inputStream = new ByteArrayInputStream(data);
					BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
					return BitmapFactory.decodeStream(bufferedInputStream);
				}
			}
		} finally {
			cursor.close();
		}

		return null;
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public InputStream openDisplayPhoto(long contactId, Activity activity) {
		Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
		Uri displayPhotoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.DISPLAY_PHOTO);
		try {
			AssetFileDescriptor fd = activity.getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
			return fd.createInputStream();
		} catch (IOException e) {
			return null;
		}
	}

	public Bitmap openPhotoBitmap(long contactId, Activity activity) {
		Uri uri = ContentUris.withAppendedId(People.CONTENT_URI, contactId);
		return People.loadContactPhoto(activity, uri, R.drawable.ic_info, null);
	}

	public Bitmap loadContactPhoto(long contactId, Activity activity) {
		Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
		InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(activity.getContentResolver(), uri);
		if (input == null) {
			return null;
		}
		return BitmapFactory.decodeStream(input);
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public Bitmap openPhotoNonTestato(long contactId, Activity activity) {
		Uri my_contact_Uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactId));
		InputStream photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(activity.getContentResolver(), my_contact_Uri, true);
		BufferedInputStream buf = new BufferedInputStream(photo_stream);
		Bitmap my_btmp = BitmapFactory.decodeStream(buf);
		try {
			buf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return my_btmp;
	}
}
