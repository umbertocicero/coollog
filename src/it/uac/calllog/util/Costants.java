package it.uac.calllog.util;


public class Costants {

	public static final String EXTRA_CALL = "call_object";
	public static final String PREF_LAST_CALL_TAB = "PREF_LAST_CALL_TAB";
	public static final String PREF_ORDER_BY = "PREF_ORDER_BY";
	public static final String PREF_LAST_TAB = "PREF_LAST_TAB";
	public static final String PREF_SINGLE_CLICK = "PREF_SINGLE_CLICK";
	public static final String PREF_ZOOM_ENABLED = "settings_enable_zoom";
	public static final String PREF_MOVE_ENABLED = "settings_enable_move";
	public static final String PREF_DISPLAY_NAME = "settings_enable_display_name";
	public static final Boolean PREF_SINGLE_CLICK_DEF = true;
	public static final Boolean PREF_ZOOM_ENABLED_DEF = true;
	public static final Boolean PREF_DISPLAY_NAME_DEF = true;
	public static final Boolean PREF_MOVE_ENABLED_DEF = false;
	
	public static final int TAB_CALL=0;
	public static final int TAB_STATISTICS=1;
	
	public static final int ALL = 1;
	public static final int MISSED_TYPE = 2;
	public static final int INCOMING_TYPE = 3;
	public static final int OUTGOING_TYPE = 4;
	public static final int IN_VS_OUT_TYPE = 5;
	public static final int CHART_NUMBER = 6;
	public static final int CHART_DURATION = 7;
	
	public static final String STATISTICS_TYPE = "STATISTICS_TYPE";
	public static final String STATISTICS_TYPE_NAME = "STATISTICS_TYPE_NAME";
	
	public static final String DATA_FORMAT = "dd/MM/yy HH:mm:ss";
	public static final String DATA_FORMAT_NO_YEAR = "dd/MM HH:mm:ss";

	public static final int PREF_DEFAULT_TAB = ALL;
	
	public final static String SHOW_DIALOG = "it.uac.calllog.SHOW_DIALOG";
	public final static String ACCESS_NUMBER = "it.uac.calllog.ACCESS_NUMBER";
	
	public final static String APP_URL = "https://play.google.com/store/apps/details?id=it.uac.calllog";
	public final static String PS_METADATA_FETCHER_URL = "http://www.iosue.it/federico/apps/PSMetadataFetcher/get_app_data.php?url=";
	
	public final static long UPDATE_MIN_FREQUENCY = 24*60*60*1000; // 1 day
	public final static String PREF_LAST_UPDATE_CHECK = "last_update_check";
}
