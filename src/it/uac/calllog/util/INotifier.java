package it.uac.calllog.util;

public interface INotifier {
	
    public void notify(Object data);
}