package it.uac.calllog;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;

public class BaseActivity extends ActionBarActivity{
	
	protected SharedPreferences prefs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
	}
	
	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return super.onSupportNavigateUp();
	}
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public boolean onNavigateUp() {
		// TODO Auto-generated method stub
		onBackPressed();
		return super.onNavigateUp();
	}
}
