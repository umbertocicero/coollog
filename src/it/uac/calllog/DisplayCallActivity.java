package it.uac.calllog;

import it.uac.calllog.adapter.CallAdapter;
import it.uac.calllog.bo.CallObject;
import it.uac.calllog.fragment.StatDialogFragment;
import it.uac.calllog.singleton.MapSingleton;
import it.uac.calllog.util.Costants;
import it.uac.calllog.util.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class DisplayCallActivity extends BaseActivity {
	CallObject callObj = null;
	SimpleDateFormat sdf = new SimpleDateFormat(Costants.DATA_FORMAT, Locale.getDefault());
	private ListView listView = null;
	private SparseArray<ArrayList<CallObject>> map;
	TextView name;
	TextView number;
	TextView callDate;
	TextView dayOfWeek;
	TextView callDuration;
	TextView callType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_detail);

		Intent intent = getIntent();
		callObj = (CallObject) intent.getSerializableExtra(Costants.EXTRA_CALL);
		setTitle(callObj.getName());
		createView();
	}

	@SuppressLint("NewApi")
	private void createView() {
		name = (TextView) findViewById(R.id.name);
		number = (TextView) findViewById(R.id.number);
		callDate = (TextView) findViewById(R.id.callDate);
		dayOfWeek = (TextView) findViewById(R.id.dayOfWeek);
		callDuration = (TextView) findViewById(R.id.callDuration);
		callType = (TextView) findViewById(R.id.callType);

		setTextValue(callObj);
		

		listView = (ListView) findViewById(R.id.listView1);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			listView.setSelector(R.drawable.selector_listview);
		} else {
			listView.setSelector(R.drawable.selector_listview_trasparent);
			listView.setBackgroundColor(Color.TRANSPARENT);
			listView.setCacheColorHint(Color.TRANSPARENT);
		}

		map = MapSingleton.getInstance().getMap(this);
		ArrayList<CallObject> temp = map.get(Costants.ALL);
		ArrayList<CallObject> results = new ArrayList<CallObject>();
		for (CallObject callObject : temp) {
			if (callObject.getName().equals(this.callObj.getName()))
				results.add(callObject);
		}

		final CallAdapter adapter = new CallAdapter(this, true, 0, android.R.layout.simple_list_item_1, results);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> listView, View itemView, int position, long itemId) {
				CallObject call = (CallObject) listView.getItemAtPosition(position);
				setTextValue(call);

				adapter.setSelectedItem(position);
				adapter.notifyDataSetChanged();// To refresh item inside list view

			}
		});
		final QuickContactBadge badgeSmall = (QuickContactBadge) findViewById(R.id.quickContactBadge1);
		badgeSmall.setMode(ContactsContract.QuickContact.MODE_MEDIUM);
		badgeSmall.assignContactFromPhone(callObj.getPhNumber(), true);
		
		if (callObj.getIdPhoto() != null) {
			AsyncTask<Void, Void, Bitmap> task = new AsyncTask<Void, Void, Bitmap>() {

				@Override
				protected Bitmap doInBackground(Void... params) {
					Utilities u = new Utilities();
					Bitmap bitmapPhoto = null;
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						bitmapPhoto = u.openPhoto(callObj.getIdPhoto(), DisplayCallActivity.this);
					} else {
						bitmapPhoto = u.loadContactPhoto(callObj.getIdPhoto(), DisplayCallActivity.this);
					}
					return bitmapPhoto;
				}

				@Override
				protected void onPostExecute(Bitmap bitmapPhoto) {
					if (bitmapPhoto != null) {
						badgeSmall.setImageBitmap(bitmapPhoto);
					} else {
						badgeSmall.setImageResource(R.drawable.ic_contact);
					}
				};
			};
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				task.execute();
			}
		} else {
			badgeSmall.setImageResource(R.drawable.ic_contact);
		}
	}


	private void setTextValue(CallObject callObj) {
		name.setText(callObj.getName().equals(callObj.getPhNumber()) ? getString(R.string.unknown) : callObj.getName());
		number.setText(callObj.getPhNumber());
		GregorianCalendar greg = new GregorianCalendar();
		long longCalDate = Long.valueOf(callObj.getCallDate());
		greg.setTimeInMillis(longCalDate);
		callDate.setText(sdf.format(longCalDate));
		dayOfWeek.setText(Utilities.switchDayOfWeek(greg, getResources()));
		callDuration.setText(Utilities.formatTime(Long.valueOf(callObj.getCallDuration()), true));
		callType.setText(callObj.getDir());
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.call_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		String url = "";
		switch (item.getItemId()) {
		case R.id.action_call:
			url = "tel:" + callObj.getPhNumber();
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
			startActivity(intent);
			return true;
		case R.id.action_sms:
			url = "sms:" + callObj.getPhNumber();
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			return true;
		case R.id.action_statistics:
			DialogFragment newFragment = StatDialogFragment.newInstance(callObj.getName());
		    newFragment.show(getSupportFragmentManager(), "dialog");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
