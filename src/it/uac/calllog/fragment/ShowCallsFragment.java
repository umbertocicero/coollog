package it.uac.calllog.fragment;

import it.uac.calllog.DisplayCallActivity;
import it.uac.calllog.R;
import it.uac.calllog.adapter.CallAdapter;
import it.uac.calllog.bo.CallObject;
import it.uac.calllog.comparator.DateComparator;
import it.uac.calllog.singleton.MapSingleton;
import it.uac.calllog.util.Costants;
import it.uac.calllog.util.INotifier;
import it.uac.calllog.util.Utilities;

import java.util.ArrayList;
import java.util.Collections;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;

public class ShowCallsFragment extends Fragment implements INotifier {
	private ListView listView = null;
	private SparseArray<ArrayList<CallObject>> map;
	private View rootView;
	private Button allButton;
	private Button missedButton;
	private Button inButton;
	private Button outButton;
	private CallAdapter adapter;
	private int lastTab;

	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	private String orderType;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Utilities.notifierTabCalls = this;
		rootView = inflater.inflate(R.layout.fragment_show_all, container, false);
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		orderType = prefs.getString(Costants.PREF_ORDER_BY, DateComparator.DESC);
		setHasOptionsMenu(true);

		lastTab = prefs.getInt(Costants.PREF_LAST_CALL_TAB, Costants.PREF_DEFAULT_TAB);
		connectView();
		setButtonColor();
		if (adapter == null)
			onRefresh();

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> listView, View itemView, int position, long itemId) {
				CallObject call = (CallObject) listView.getItemAtPosition(position);
				Intent intent = new Intent(getActivity(), DisplayCallActivity.class);
				intent.putExtra(Costants.EXTRA_CALL, call);
				startActivity(intent);
			}
		});
		return rootView;
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		if (orderType.equals(DateComparator.DESC)) {
			menu.findItem(R.id.action_order_desc).setVisible(false);
			menu.findItem(R.id.action_order_asc).setVisible(true);
		} else {
			menu.findItem(R.id.action_order_desc).setVisible(true);
			menu.findItem(R.id.action_order_asc).setVisible(false);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.showall_fragment_actions, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_order_desc:
			adapter.sort(DateComparator.DESC);
			orderType = DateComparator.DESC;
			break;
		case R.id.action_order_asc:
			adapter.sort(DateComparator.ASC);
			orderType = DateComparator.ASC;
			break;
		default:
			break;
		}
		getActivity().supportInvalidateOptionsMenu();
		editor = prefs.edit();
		editor.putString(Costants.PREF_ORDER_BY, orderType);
		editor.commit();
		return true;
	}

	@SuppressLint("NewApi")
	private void onRefresh() {

		AsyncTask<Void, ArrayList<CallObject>, ArrayList<CallObject>> task = new AsyncTask<Void, ArrayList<CallObject>, ArrayList<CallObject>>() {

			@SuppressWarnings("unchecked")
			@Override
			protected ArrayList<CallObject> doInBackground(Void... params) {
				map = MapSingleton.getInstance().getMap(getActivity());
				ArrayList<CallObject> results = map.get(lastTab);
				Collections.sort(results, new DateComparator(orderType));
				ArrayList<CallObject> resultsClone = (ArrayList<CallObject>) results.clone();
				return resultsClone;
			}

			@Override
			protected void onPostExecute(ArrayList<CallObject> result) {
				if (result != null) {
					adapter = new CallAdapter(getActivity(), false, null, android.R.layout.simple_list_item_1, result);
					listView.setAdapter(adapter);
				}
			}
		};
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			task.execute();
		}
	}

	public void connectView() {
		listView = (ListView) rootView.findViewById(R.id.listView1);
		allButton = (Button) rootView.findViewById(R.id.allButton);
		missedButton = (Button) rootView.findViewById(R.id.missedButton);
		inButton = (Button) rootView.findViewById(R.id.incomingButton);
		outButton = (Button) rootView.findViewById(R.id.outcomingButton);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			TableLayout tab = (TableLayout) rootView.findViewById(R.id.table);
			tab.setBackgroundResource(R.drawable.shape_blue_white);
		}

		allButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changeAdapterValue(Costants.ALL, allButton);
			}
		});
		missedButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changeAdapterValue(Costants.MISSED_TYPE, missedButton);
			}
		});
		inButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changeAdapterValue(Costants.INCOMING_TYPE, inButton);
			}
		});
		outButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changeAdapterValue(Costants.OUTGOING_TYPE, outButton);
			}
		});

		listView = (ListView) rootView.findViewById(R.id.listView1);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			listView.setSelector(R.drawable.selector_listview);
		}
		if (adapter != null)
			listView.setAdapter(adapter);
	}

	private void changeAdapterValue(int dir, Button button) {
		if (adapter != null) {
			adapter.clear();
			ArrayList<CallObject> ret = map.get(dir);
			Collections.sort(ret, new DateComparator(orderType));
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				adapter.addAll(ret);
			} else {
				for (CallObject i : ret) {
					adapter.add(i);
				}
			}
			lastTab = dir;

			allButton.setTextAppearance(getActivity(), R.style.DefaultTableText);
			missedButton.setTextAppearance(getActivity(), R.style.DefaultTableText);
			inButton.setTextAppearance(getActivity(), R.style.DefaultTableText);
			outButton.setTextAppearance(getActivity(), R.style.DefaultTableText);
			button.setTextAppearance(getActivity(), R.style.BluHUGETableText);
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		saveLastTab();
		if (rootView != null) {
			ViewGroup parentViewGroup = (ViewGroup) rootView.getParent();
			if (parentViewGroup != null) {
				parentViewGroup.removeAllViews();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		saveLastTab();
	};

	private void saveLastTab() {
		editor = prefs.edit();
		editor.putInt(Costants.PREF_LAST_CALL_TAB, lastTab);
		editor.putInt(Costants.PREF_LAST_TAB, Costants.TAB_CALL);
		editor.commit();
	}

	private void setButtonColor() {
		Button generic;
		switch (lastTab) {
		case Costants.ALL:
			generic = allButton;
			break;
		case Costants.MISSED_TYPE:
			generic = missedButton;
			break;
		case Costants.INCOMING_TYPE:
			generic = inButton;
			break;
		case Costants.OUTGOING_TYPE:
			generic = outButton;
			break;
		default:
			generic = allButton;
			break;
		}
		generic.setTextAppearance(getActivity(), R.style.BluHUGETableText);
	}

	@Override
	public void notify(Object data) {
		onRefresh();
	}
}
