package it.uac.calllog.fragment;

import it.uac.calllog.DisplayChartActivity;
import it.uac.calllog.R;
import it.uac.calllog.adapter.StatisticsAdapter;
import it.uac.calllog.bo.CallObject;
import it.uac.calllog.bo.DataObject;
import it.uac.calllog.singleton.MapSingleton;
import it.uac.calllog.util.Costants;
import it.uac.calllog.util.INotifier;
import it.uac.calllog.util.Utilities;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class StatisticsFragment extends Fragment implements INotifier {
	private View rootView;
	private SparseArray<ArrayList<CallObject>> map;
	ArrayList<DataObject> dataObjectList;

	private ListView listView;
	private StatisticsAdapter adapter;

	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Utilities.notifierTabStats = this;
		rootView = inflater.inflate(R.layout.fragment_statistics, container, false);
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		connectView();
		dataObjectList = new ArrayList<DataObject>();
		adapter = new StatisticsAdapter(getActivity(), null, android.R.layout.simple_list_item_1, dataObjectList);
		onRefresh();

		listView.setAdapter(adapter);

		return rootView;
	}

	@SuppressLint("NewApi")
	private void onRefresh() {
		map = MapSingleton.getInstance().getMap(getActivity());

		dataObjectList.clear();
		dataObjectList.add(new DataObject(getString(R.string.all), Long.valueOf(map.get(Costants.ALL).size())));
		dataObjectList.add(new DataObject(getString(R.string.missed), Long.valueOf(map.get(Costants.MISSED_TYPE).size())));
		dataObjectList.add(new DataObject(getString(R.string.incoming), Long.valueOf(map.get(Costants.INCOMING_TYPE).size())));
		dataObjectList.add(new DataObject(getString(R.string.outcoming), Long.valueOf(map.get(Costants.OUTGOING_TYPE).size())));
		dataObjectList.add(new DataObject(getString(R.string.in_vs_out), null));
		adapter.notifyDataSetChanged();// To refresh item inside list view
	}

	public void connectView() {

		listView = (ListView) rootView.findViewById(R.id.listView1);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				goToChart(position);
			}

		});

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			listView.setSelector(R.drawable.selector_listview);
		}

	}

	private void goToChart(int position) {

		Intent intent = new Intent(getActivity(), DisplayChartActivity.class);
		String contextName="";
		switch (position) {
		case 0:
			intent.putExtra(Costants.STATISTICS_TYPE, Costants.ALL);
			contextName=getString(R.string.all);
			break;
		case 1:
			intent.putExtra(Costants.STATISTICS_TYPE, Costants.MISSED_TYPE);
			contextName=getString(R.string.missed);
			break;
		case 2:
			intent.putExtra(Costants.STATISTICS_TYPE, Costants.INCOMING_TYPE);
			contextName=getString(R.string.incoming);
			break;
		case 3:
			intent.putExtra(Costants.STATISTICS_TYPE, Costants.OUTGOING_TYPE);
			contextName=getString(R.string.outcoming);
			break;
		case 4:
			intent.putExtra(Costants.STATISTICS_TYPE, Costants.IN_VS_OUT_TYPE);
			contextName=getString(R.string.in_vs_out);
			break;
		default:
			intent.putExtra(Costants.STATISTICS_TYPE, Costants.ALL);
			contextName=getString(R.string.all);
			break;
		}
		prefs.edit().putString(Costants.STATISTICS_TYPE_NAME, contextName).commit();
		startActivity(intent);

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		saveLastTab();
		if (rootView != null) {
			ViewGroup parentViewGroup = (ViewGroup) rootView.getParent();
			if (parentViewGroup != null) {
				parentViewGroup.removeAllViews();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		saveLastTab();
	};

	private void saveLastTab() {
		editor = prefs.edit();
		editor.putInt(Costants.PREF_LAST_TAB, Costants.TAB_STATISTICS);
		editor.commit();
	}

	@Override
	public void notify(Object data) {
		onRefresh();
	}

}
