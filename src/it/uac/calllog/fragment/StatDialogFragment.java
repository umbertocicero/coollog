package it.uac.calllog.fragment;

import it.uac.calllog.R;
import it.uac.calllog.asyncTask.StatisticsTask;
import it.uac.calllog.bo.CallObject;
import it.uac.calllog.singleton.MapSingleton;
import it.uac.calllog.util.Costants;
import it.uac.calllog.util.Utilities;

import java.util.ArrayList;

import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.DialogFragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class StatDialogFragment extends DialogFragment {
	TextView statAll;
	TextView statMissed;
	TextView statIn;
	TextView statOut;
	TextView statTotLength;
	TextView statTotInLength;
	TextView statTotOutLength;
	TextView statMaxOut;
	TextView statMaxIn;

	String name;

	public static StatDialogFragment newInstance(String name) {
		StatDialogFragment frag = new StatDialogFragment();
		Bundle args = new Bundle();
		args.putString("name", name);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int style = DialogFragment.STYLE_NO_TITLE;
		int theme = 0;
		// int theme = android.R.style.Theme_Holo_Light_Dialog;
		setStyle(style, theme);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.popup_statistics, container, false);

		Bundle bundle = getArguments();
		name = bundle.getString("name");

		statAll = (TextView) v.findViewById(R.id.statAll);
		statMissed = (TextView) v.findViewById(R.id.statMissed);
		statIn = (TextView) v.findViewById(R.id.statIn);
		statOut = (TextView) v.findViewById(R.id.statOut);
		statTotLength = (TextView) v.findViewById(R.id.statTotLength);
		statTotInLength = (TextView) v.findViewById(R.id.statTotInLength);
		statTotOutLength = (TextView) v.findViewById(R.id.statTotOutLength);
		statMaxOut = (TextView) v.findViewById(R.id.statMaxOut);
		statMaxIn = (TextView) v.findViewById(R.id.statMaxIn);
		SparseArray<ArrayList<CallObject>> map = MapSingleton.getInstance().getMap(getActivity());
		new StatisticsTask(statAll, map.get(Costants.ALL), name).execute();
		new StatisticsTask(statMissed, map.get(Costants.MISSED_TYPE), name).execute();
		new StatisticsTask(statIn, map.get(Costants.INCOMING_TYPE), name).execute();
		new StatisticsTask(statOut, map.get(Costants.OUTGOING_TYPE), name).execute();
		String currentName = null;
		Long totTime = 0l;
		Long totTimeIn = 0l;
		Long totTimeOut = 0l;
		Long maxTimeIn = 0l;
		Long maxTimeOut = 0l;
		for (CallObject callObject : map.get(Costants.ALL)) {
			currentName = callObject.getName();
			if (currentName != null && currentName.equals(name)) {
				Long currentDuration = Long.valueOf(callObject.getCallDuration());
				if (callObject.getCallType() == CallLog.Calls.OUTGOING_TYPE) {
					totTimeIn += currentDuration;
					if (currentDuration > maxTimeIn) {
						maxTimeIn = currentDuration;
					}
				} else if (callObject.getCallType() == CallLog.Calls.INCOMING_TYPE) {
					totTimeOut += currentDuration;
					if (currentDuration > maxTimeOut) {
						maxTimeOut = currentDuration;
					}
				}
			}
		}
		totTime = totTimeIn + totTimeOut;
		String totTimeString = Utilities.formatTime(totTime, true);
		String totTimeInString = Utilities.formatTime(totTimeIn, true);
		String totTimeOutString = Utilities.formatTime(totTimeOut, true);
		String maxTimeOutString = Utilities.formatTime(maxTimeOut, true);
		String maxTimeInString = Utilities.formatTime(maxTimeIn, true);
		statTotLength.setText(totTimeString);
		statTotInLength.setText(totTimeInString);
		statTotOutLength.setText(totTimeOutString);
		statMaxOut.setText(maxTimeOutString);
		statMaxIn.setText(maxTimeInString);

		return v;
	}
}