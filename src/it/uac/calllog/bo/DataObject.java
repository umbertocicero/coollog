package it.uac.calllog.bo;

public class DataObject {
	private String name;
	private Long value;
	private double pecentage;

	public DataObject() {
		super();
	}

	public DataObject(String name, Long value) {
		this.name = name;
		this.value = value;
	}

	public DataObject(String name, Long value, double pecentage) {
		super();
		this.name = name;
		this.value = value;
		this.pecentage = pecentage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public double getPecentage() {
		return pecentage;
	}

	public void setPecentage(double pecentage) {
		this.pecentage = pecentage;
	}
}
