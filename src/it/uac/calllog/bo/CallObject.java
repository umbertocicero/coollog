package it.uac.calllog.bo;

import java.io.Serializable;
import java.util.Date;

public class CallObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3866920223638551446L;
	private String name;
	private String contactId;
	private String phNumber;
	private Integer callType;
	private Integer idPhoto;
	private String callDate;
	private Date callDayTime;
	private String callDuration;
	private String dir;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhNumber() {
		return phNumber;
	}

	public void setPhNumber(String phNumber) {
		this.phNumber = phNumber;
	}

	public Integer getCallType() {
		return callType;
	}

	public void setCallType(Integer callType) {
		this.callType = callType;
	}

	public String getCallDate() {
		return callDate;
	}

	public void setCallDate(String callDate) {
		this.callDate = callDate;
	}

	public Date getCallDayTime() {
		return callDayTime;
	}

	public void setCallDayTime(Date callDayTime) {
		this.callDayTime = callDayTime;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public Integer getIdPhoto() {
		return idPhoto;
	}

	public void setIdPhoto(Integer idPhoto) {
		this.idPhoto = idPhoto;
	}

}
