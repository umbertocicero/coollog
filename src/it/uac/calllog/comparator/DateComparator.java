package it.uac.calllog.comparator;

import it.uac.calllog.bo.CallObject;

import java.util.Comparator;

public class DateComparator implements Comparator<CallObject>{
public static final String DESC="DESC";
public static final String ASC="ASC";
	private String orderType;

	public DateComparator(String type) {

	    this.orderType = type;

	}
	@Override
	public int compare(CallObject lhs, CallObject rhs) {
		int res = 0;
		if(orderType.equals(ASC))
			res = (Long.valueOf(lhs.getCallDate())).compareTo(Long.valueOf(rhs.getCallDate()));
		else res = (Long.valueOf(rhs.getCallDate())).compareTo(Long.valueOf(lhs.getCallDate()));
	    return res;
	}

}
