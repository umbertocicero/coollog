package it.uac.calllog;

import android.os.Bundle;
import android.webkit.WebView;

public class InfoActivity extends BaseActivity {
	
	private WebView mWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		setTitle(getString(R.string.action_info));
		
		mWebView = (WebView) findViewById(R.id.activity_main_webview);
		mWebView.loadUrl("file:///android_asset/info.html");
	}
}
